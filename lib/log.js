/* eslint-disable no-console */
// named log methods (e.g. log.action('something', 'to', 'log', obj))
// each method describes the log name, an emoji icon to use,
// and the console method invoked if something other than the default 'log' method
const methods = {
  action: ["⚡"],
  connection: ["🔗"],
  err: ["🚨", "error"],
  error: ["🚨", "error"],
  sad: ["😭"],
  good: ["👍"],
  ignore: ["🙈"],
  now: [],
  ok: ["✅"],
  saved: ["💾"],
  warn: ["⚠️ "],
};

const log = {
  debug(...args) {
    if (!process.env.DEBUG) return;
    console.log(...args);
  },
};

Object.keys(methods).forEach((name) => {
  log[name] = (...args) => {
    const m = methods[name][1] || "log";
    methods[name][0]
      ? console[m](methods[name][0], ...args)
      : console[m](...args);
  };
});

module.exports = log;
